# GroupProject_Database

# Projected and Coded by:
    Vinicius Bastos #2239384
    Chiril Barba    #2232864

# USER ASSUMPTIONS
    1. We assume that the user will enter the right data inside the inputs. That means that we do not have data validation for every user input.

    2. We have inserted some data validation. Such were created to help the user with their inputs. 
        2.1 For example, we have inserted a REGEX validation for the insertion of a date. That way the user will know more easily what date format is accepted.

# Organization
    1. We always have 2 menus.
        2. The first is the main menu which sets the user to choose in which package they will work on.
        3. The second menu is the package menu that the user chose to work on. There, they will have all management options inside the package.

