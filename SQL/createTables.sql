DROP TABLE Customers CASCADE CONSTRAINTS;
DROP TABLE Products CASCADE CONSTRAINTS;
DROP TABLE Stores CASCADE CONSTRAINTS;
DROP TABLE ProductStore CASCADE CONSTRAINTS;
DROP TABLE Reviews CASCADE CONSTRAINTS;
DROP TABLE Orders CASCADE CONSTRAINTS;
DROP TABLE Shipping CASCADE CONSTRAINTS;
DROP TABLE Warehouses CASCADE CONSTRAINTS;
DROP TABLE Warehouses_Products CASCADE CONSTRAINTS;

CREATE TABLE Customers(
    customer_id         NUMBER(10) PRIMARY KEY,
    customer_name       VARCHAR2(100),
    customer_email      VARCHAR2(100)
);

CREATE TABLE Products(
    product_id          NUMBER(10) PRIMARY KEY,
    product_name        VARCHAR2(100),
    product_price       NUMBER(10,2),
    product_category    VARCHAR2(50),
    product_review      NUMBER(1)
);

CREATE TABLE Stores(
    store_id            NUMBER(10) PRIMARY KEY,
    store_name          VARCHAR2(100)
);

CREATE TABLE ProductStore(
    product_id NUMBER(10)   REFERENCES Products(product_id),
    store_id NUMBER(10)     REFERENCES Stores(store_id)
);

CREATE TABLE Reviews(
    review_id               NUMBER(10) PRIMARY KEY,
    customer_id             NUMBER(10) REFERENCES Customers (customer_id),
    product_id              NUMBER(10) REFERENCES Products (product_id),
    review_flag             NUMBER(1),
    review_description      VARCHAR2(500)
);

CREATE TABLE Shipping(
    order_id            NUMBER(10) PRIMARY KEY,
    customer_id         NUMBER(10) REFERENCES Customers(customer_id),
    customer_address    VARCHAR2(200),
    order_date          DATE
);

CREATE TABLE Orders(
    order_id        NUMBER(10) REFERENCES Shipping(order_id),
    product_id      NUMBER(10) REFERENCES Products(product_id),
    quantity        NUMBER(5)
);

CREATE TABLE Warehouses(
    warehouse_id        NUMBER(6)       PRIMARY KEY,
    warehouse_name      VARCHAR2(11),
    w_address           VARCHAR2(200)
);

CREATE TABLE Warehouses_Products(
    warehouse_id    NUMBER(6)  REFERENCES Warehouses(warehouse_id),
    product_id      NUMBER(10) REFERENCES Products(product_id),
    quantity        NUMBER(6)
);