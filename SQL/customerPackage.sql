CREATE OR REPLACE PACKAGE customerPackage AS

    PROCEDURE addCustomer(customer_name IN Customers.customer_name%TYPE,customer_email IN Customers.customer_email%TYPE);
        -- adds a new customer
        
    PROCEDURE modifyCustomer(cid IN Customers.customer_id%TYPE,cname IN Customers.customer_name%TYPE,cemail IN Customers.customer_email%TYPE);
        -- modifies an existing customer
        
    PROCEDURE deleteCustomer(cid IN Customers.customer_id%TYPE);
        -- deletes an existing customer

END customerPackage;
/

CREATE OR REPLACE PACKAGE BODY customerPackage AS
    --Private function that determines the next id
    FUNCTION getNextCustomerId RETURN Customers.customer_id%TYPE
    AS 
        newid NUMBER;
    BEGIN
        SELECT COALESCE(MAX(customer_id), 0) + 1 INTO newid
        FROM Customers;
        RETURN newid;
    END getNextCustomerId;
    --Public procedure that adds a new customer to the db
    PROCEDURE addCustomer(customer_name IN Customers.customer_name%TYPE, 
                        customer_email IN Customers.customer_email%TYPE)
    AS 
        newid Customers.customer_id%TYPE;
    BEGIN
        newid := getNextCustomerId;
        INSERT INTO Customers(customer_id, customer_name, customer_email)
        VALUES (newid, customer_name, customer_email);
        COMMIT;
    END addCustomer;
    --Public Procedure that modifies Customers
    PROCEDURE modifyCustomer(cid IN Customers.customer_id%TYPE,
                            cname IN Customers.customer_name%TYPE, 
                            cemail IN Customers.customer_email%TYPE)
    AS 
    BEGIN
        UPDATE Customers
        SET customer_name = cname, customer_email = cemail
        WHERE customer_id = cid;
    END;
    --Public Procedure that deletes a Customer
    PROCEDURE deleteCustomer(cid IN Customers.customer_id%TYPE)
    AS 
    BEGIN
        -- Delete from Orders
        DELETE FROM Orders
        WHERE order_id IN (SELECT order_id FROM Shipping WHERE customer_id = cid);

        -- Delete from Reviews
        DELETE FROM Reviews
        WHERE customer_id = cid;

        -- Delete from Shipping
        DELETE FROM Shipping
        WHERE customer_id = cid;

        -- Delete from Customers (parent table)
        DELETE FROM Customers
        WHERE customer_id = cid;
    END;
END customerPackage;
/

--BEGIN
    --customerPackage.addCustomer('Jhonny Test','eggs@gmail.com');
    --customerPackage.modifyCustomer(13,'John Doe', 'legs@game.com');
    --customerPackage.deleteCustomer(13);
--END;
