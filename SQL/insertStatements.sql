-- Insert data into Customers table
INSERT INTO Customers (customer_id, customer_name, customer_email)
VALUES (1, 'Mahsa Sadeghi', 'msadeghi@dawsoncollege.qc.ca');
INSERT INTO Customers (customer_id, customer_name, customer_email)
VALUES (2, 'Alex Brown', 'alex@gmail.com');
INSERT INTO Customers (customer_id, customer_name, customer_email)
VALUES (3, 'Martin Alexandre', 'marting@yahoo.com');
INSERT INTO Customers (customer_id, customer_name, customer_email)
VALUES (4, 'Daneil Hanne', 'daneil@yahoo.com');
INSERT INTO Customers (customer_id, customer_name, customer_email)
VALUES (5, 'John Boura', 'bdoura@gmail.com');
INSERT INTO Customers (customer_id, customer_name, customer_email)
VALUES (6, 'Ari Brown', 'b.a@gmail.com');
INSERT INTO Customers (customer_id, customer_name, customer_email)
VALUES (7, 'Amanda Harry', 'am.harry@yahioo.com');
INSERT INTO Customers (customer_id, customer_name, customer_email)
VALUES (8, 'Jack Jonhson', 'johnson.a@gmail.com');
INSERT INTO Customers (customer_id, customer_name, customer_email)
VALUES (9, 'John Belle', 'abcd@yahoo.com');
INSERT INTO Customers (customer_id, customer_name, customer_email)
VALUES (10, 'Martin Li', 'm.li@gmail.com');
INSERT INTO Customers (customer_id, customer_name, customer_email)
VALUES (11, 'Olivia Smith', 'smith@hotmail.com');
INSERT INTO Customers (customer_id, customer_name, customer_email)
VALUES (12, 'Noah Garcia', 'g.noah@yahoo.com');

--Inserting products
INSERT INTO Products (product_id, product_name, product_price, product_category, product_review)
VALUES (1, 'laptop ASUS 104S', 970.00, 'electronics', 4);
INSERT INTO Products (product_id, product_name, product_price, product_category, product_review)
VALUES (2, 'apple', 5.00, 'Grocery', 3);
INSERT INTO Products (product_id, product_name, product_price, product_category, product_review)
VALUES (3, 'SIMS CD', 16.67, 'Video Games', 2);
INSERT INTO Products (product_id, product_name, product_price, product_category, product_review)
VALUES (4, 'orange', 2.00, 'grocery', 5);
INSERT INTO Products (product_id, product_name, product_price, product_category, product_review)
VALUES (5, 'Barbie Movie', 30.00, 'DVD', 1);
INSERT INTO Products (product_id, product_name, product_price, product_category, product_review)
VALUES (6, 'LOreal Normal Hair', 10.00, 'Health', 1);
INSERT INTO Products (product_id, product_name, product_price, product_category, product_review)
VALUES (7, 'BMW iX Lego', 40.00, 'Toys', 1);
INSERT INTO Products (product_id, product_name, product_price, product_category, product_review)
VALUES (8, 'BMW i6', 50000.00, 'Cars', 5);
INSERT INTO Products (product_id, product_name, product_price, product_category, product_review)
VALUES (9, 'Truck 500c', 856600.00, 'Vehicle', 2);
INSERT INTO Products (product_id, product_name, product_price, product_category, product_review)
VALUES (10, 'paper towel', 16.67, 'Beauty', 5);
INSERT INTO Products (product_id, product_name, product_price, product_category, product_review)
VALUES (11, 'plum', 1.67, 'grocery', 4);
INSERT INTO Products (product_id, product_name, product_price, product_category, product_review)
VALUES (12, 'Lamborghini Lego', 40.00, 'Toys', 1);
INSERT INTO Products (product_id, product_name, product_price, product_category, product_review)
VALUES (13, 'chicken', 9.50, 'grocery', 4);
INSERT INTO Products (product_id, product_name, product_price, product_category, product_review)
VALUES (14, 'pasta', 4.50, 'grocery', 5);
INSERT INTO Products (product_id, product_name, product_price, product_category, product_review)
VALUES (15, 'PS5', 200.00, 'electronics', NULL);
INSERT INTO Products (product_id, product_name, product_price, product_category, product_review)
VALUES (16, 'tomato', NULL, NULL, NULL);
INSERT INTO Products (product_id, product_name, product_price, product_category, product_review)
VALUES (17, 'Train X745', NULL, NULL, NULL);

--insert into Stores
INSERT INTO Stores (store_id, store_name)
VALUES (1, 'marche adonis');
INSERT INTO Stores (store_id, store_name)
VALUES (2, 'marche atwater');
INSERT INTO Stores (store_id, store_name)
VALUES (3, 'dawson store');
INSERT INTO Stores (store_id, store_name)
VALUES (4, 'store magic');
INSERT INTO Stores (store_id, store_name)
VALUES (5, 'movie store');
INSERT INTO Stores (store_id, store_name)
VALUES (6, 'super rue champlain');
INSERT INTO Stores (store_id, store_name)
VALUES (7, 'toy r us');
INSERT INTO Stores (store_id, store_name)
VALUES (8, 'Dealer one');
INSERT INTO Stores (store_id, store_name)
VALUES (9, 'dealer montreal');
INSERT INTO Stores (store_id, store_name)
VALUES (10, 'movie start');
INSERT INTO Stores (store_id, store_name)
VALUES (11, 'star store');

--inserting productstores
INSERT INTO ProductStore (product_id, store_id)
VALUES (1, 1);  -- laptop ASUS 104S at marche adonis
INSERT INTO ProductStore (product_id, store_id)
VALUES (2, 2);  -- apple at marche atwater
INSERT INTO ProductStore (product_id, store_id)
VALUES (3, 3);  -- SIMS CD at dawson store
INSERT INTO ProductStore (product_id, store_id)
VALUES (4, 4);  -- orange at store magic
INSERT INTO ProductStore (product_id, store_id)
VALUES (5, 5);  -- Barbie Movie at movie store
INSERT INTO ProductStore (product_id, store_id)
VALUES (6, 6);  -- L'Oreal Normal Hair at super rue champlain
INSERT INTO ProductStore (product_id, store_id)
VALUES (7, 7);  -- BMW iX Lego at toy r us
INSERT INTO ProductStore (product_id, store_id)
VALUES (8, 8);  -- BMW i6 at Dealer one
INSERT INTO ProductStore (product_id, store_id)
VALUES (9, 9);  -- Truck 500c at dealer montreal
INSERT INTO ProductStore (product_id, store_id)
VALUES (10, 10);  -- paper towel at movie start
INSERT INTO ProductStore (product_id, store_id)
VALUES (11, 2);  -- plum at marche atwater
INSERT INTO ProductStore (product_id, store_id)
VALUES (12, 7);  -- Lamborghini Lego at toy r us
INSERT INTO ProductStore (product_id, store_id)
VALUES (13, 1);  -- Chicken at marche adonis
INSERT INTO ProductStore (product_id, store_id)
VALUES (14, 2);  -- Pasta at marche atwater
INSERT INTO ProductStore (product_id, store_id)
VALUES (15, 11);  -- PS5 at star store
INSERT INTO ProductStore (product_id, store_id)
VALUES (14, 4);  -- Pasta at store magic
INSERT INTO ProductStore (product_id, store_id)
VALUES (16, 1);  -- Tomato at marche adonis
INSERT INTO ProductStore (product_id, store_id)
VALUES (17, 4);  -- Train X745 at store magic

--inserting into orders, shipping, eviews and warehouses
INSERT INTO Reviews (review_id, customer_id, product_id, review_flag, review_description)
VALUES(1, 1, 1, 0, 'it was affordable.');
INSERT INTO Reviews (review_id, customer_id, product_id, review_flag, review_description)
VALUES(2, 2, 2, 0, 'quality was not good');
INSERT INTO Reviews (review_id, customer_id, product_id, review_flag, review_description)
VALUES(3, 3, 3, 1, NULL);
INSERT INTO Reviews (review_id, customer_id, product_id, review_flag, review_description)
VALUES(4, 4, 4, 0, 'highly recommend');
INSERT INTO Reviews (review_id, customer_id, product_id, review_flag, review_description)
VALUES(5, 2, 5, 0, NULL);
INSERT INTO Reviews (review_id, customer_id, product_id, review_flag, review_description)
VALUES(6, 3, 6, 0, 'did not worth the price');
INSERT INTO Reviews (review_id, customer_id, product_id, review_flag, review_description)
VALUES(7, 1, 7, 0, 'missing some parts');
INSERT INTO Reviews (review_id, customer_id, product_id, review_flag, review_description)
VALUES(8, 5, 8, 1, 'trash');
INSERT INTO Reviews (review_id, customer_id, product_id, review_flag, review_description)
VALUES(9, 6, 9, NULL, NULL);
INSERT INTO Reviews (review_id, customer_id, product_id, review_flag, review_description)
VALUES(10, 7, 10, NULL, NULL);
INSERT INTO Reviews (review_id, customer_id, product_id, review_flag, review_description)
VALUES(11, 8, 11, NULL, NULL);
INSERT INTO Reviews (review_id, customer_id, product_id, review_flag, review_description)
VALUES(12, 3, 12, NULL, NULL);
INSERT INTO Reviews (review_id, customer_id, product_id, review_flag, review_description)
VALUES(13, 1, 12, 0, 'missing some parts');
INSERT INTO Reviews (review_id, customer_id, product_id, review_flag, review_description)
VALUES(14, 1, 11, NULL, NULL);
INSERT INTO Reviews (review_id, customer_id, product_id, review_flag, review_description)
VALUES(15, 1, 12, 0, 'great product');
INSERT INTO Reviews (review_id, customer_id, product_id, review_flag, review_description)
VALUES(16, 9, 8, 1, 'bad quality');
INSERT INTO Reviews (review_id, customer_id, product_id, review_flag, review_description)
VALUES(17, 2, 3, 0, NULL);
INSERT INTO Reviews (review_id, customer_id, product_id, review_flag, review_description)
VALUES(18, 2, 5, 0, NULL);
INSERT INTO Reviews (review_id, customer_id, product_id, review_flag, review_description)
VALUES(19, 10, 13, NULL, NULL);
INSERT INTO Reviews (review_id, customer_id, product_id, review_flag, review_description)
VALUES(20, 11, 14, NULL, NULL);
INSERT INTO Reviews (review_id, customer_id, product_id, review_flag, review_description)
VALUES(21, 12, 15, NULL, NULL);
INSERT INTO Reviews (review_id, customer_id, product_id, review_flag, review_description)
VALUES(22, 1, 7, 2, 'worse car i have driven!');
INSERT INTO Reviews (review_id, customer_id, product_id, review_flag, review_description)
VALUES(23, 11, 14, NULL, NULL);

INSERT INTO Warehouses (warehouse_id, warehouse_name, w_address)
VALUES (1, 'Warehouse A', '100 rue William, saint laurent, Quebec, Canada');

INSERT INTO Warehouses (warehouse_id, warehouse_name, w_address)
VALUES (2, 'Warehouse B', '304 Rue François-Perrault, Villera Saint-Michel, Montréal, QC');

INSERT INTO Warehouses (warehouse_id, warehouse_name, w_address)
VALUES (3, 'Warehouse C', '86700 Weston Rd, Toronto, Canada');

INSERT INTO Warehouses (warehouse_id, warehouse_name, w_address)
VALUES (4, 'Warehouse D', '170 Sideroad, Quebec City, Canada');

INSERT INTO Warehouses (warehouse_id, warehouse_name, w_address)
VALUES (5, 'Warehouse E', '1231 Trudea road, Ottawa, Canada');

INSERT INTO Warehouses (warehouse_id, warehouse_name, w_address)
VALUES (6, 'Warehouse F', '16 Whitlock Rd, Alberta, Canada');

-- Inserting data into the Warehouses_Products table
INSERT INTO Warehouses_Products (warehouse_id, product_id, quantity)
VALUES (1, 1, 1000);

INSERT INTO Warehouses_Products (warehouse_id, product_id, quantity)
VALUES (1, 7, 10);

INSERT INTO Warehouses_Products (warehouse_id, product_id, quantity)
VALUES (1, 8, 6);

INSERT INTO Warehouses_Products (warehouse_id, product_id, quantity)
VALUES (1, 14, 2132);

INSERT INTO Warehouses_Products (warehouse_id, product_id, quantity)
VALUES (1, 16, 352222);

INSERT INTO Warehouses_Products (warehouse_id, product_id, quantity)
VALUES (2, 2, 24980);

INSERT INTO Warehouses_Products (warehouse_id, product_id, quantity)
VALUES (2, 10, 39484);

INSERT INTO Warehouses_Products (warehouse_id, product_id, quantity)
VALUES (3, 3, 103);

INSERT INTO Warehouses_Products (warehouse_id, product_id, quantity)
VALUES (3, 11, 43242);

INSERT INTO Warehouses_Products (warehouse_id, product_id, quantity)
VALUES (4, 4, 35405);

INSERT INTO Warehouses_Products (warehouse_id, product_id, quantity)
VALUES (4, 11, 6579);

INSERT INTO Warehouses_Products (warehouse_id, product_id, quantity)
VALUES (4, 15, 123);

INSERT INTO Warehouses_Products (warehouse_id, product_id, quantity)
VALUES (5, 5, 40);

INSERT INTO Warehouses_Products (warehouse_id, product_id, quantity)
VALUES (5, 9, 1000);

INSERT INTO Warehouses_Products (warehouse_id, product_id, quantity)
VALUES (5, 12, 98765);

INSERT INTO Warehouses_Products (warehouse_id, product_id, quantity)
VALUES (5, 17, 4543);

INSERT INTO Warehouses_Products (warehouse_id, product_id, quantity)
VALUES (6, 6, 450);

INSERT INTO Warehouses_Products (warehouse_id, product_id, quantity)
VALUES (6, 10, 3532);

INSERT INTO Warehouses_Products (warehouse_id, product_id, quantity)
VALUES (6, 13, 43523);
--Insert into shipping
INSERT INTO Shipping (order_id, customer_id, customer_address, order_date)
VALUES (1, 1, 'dawson college, montreal, qeuebe, canada', TO_DATE('2023-04-21', 'YYYY-MM-DD'));
INSERT INTO Shipping (order_id, customer_id, customer_address, order_date)
VALUES(2, 2, '090 boul saint laurent, montreal, quebec, canada', TO_DATE('2023-10-23', 'YYYY-MM-DD'));
INSERT INTO Shipping (order_id, customer_id, customer_address, order_date)
VALUES(3, 3, 'brossard, quebec, canada', TO_DATE('2023-10-01', 'YYYY-MM-DD'));
INSERT INTO Shipping (order_id, customer_id, customer_address, order_date)
VALUES(4, 4, '100 atwater street, toronto, canada', TO_DATE('2023-10-23', 'YYYY-MM-DD'));
INSERT INTO Shipping (order_id, customer_id, customer_address, order_date)
VALUES(5, 2, '100 Young street, toronto, canada', TO_DATE('2023-10-23', 'YYYY-MM-DD'));
INSERT INTO Shipping (order_id, customer_id, customer_address, order_date)
VALUES(6, 3, 'brossard, quebec, canada', TO_DATE('2023-10-10', 'YYYY-MM-DD'));
INSERT INTO Shipping (order_id, customer_id, customer_address, order_date)
VALUES(7, 1, 'dawson college, montreal, qeuebe, canada', TO_DATE('2023-10-11', 'YYYY-MM-DD'));
INSERT INTO Shipping (order_id, customer_id, customer_address, order_date)
VALUES(8, 5, '100 boul saint laurent, montreal, quebec, canada', TO_DATE('2023-10-10', 'YYYY-MM-DD'));
INSERT INTO Shipping (order_id, customer_id, customer_address, order_date)
VALUES(9, 6, NULL, NULL);
INSERT INTO Shipping (order_id, customer_id, customer_address, order_date)
VALUES(10, 7, '100 boul saint laurent, montreal, quebec, canada', NULL);
INSERT INTO Shipping (order_id, customer_id, customer_address, order_date)
VALUES(11, 8, 'Calgary, Alberta, Canada', TO_DATE('2020-05-06', 'YYYY-MM-DD'));
INSERT INTO Shipping (order_id, customer_id, customer_address, order_date)
VALUES(12, 3, 'brossard, quebec, canada', TO_DATE('2019-09-12', 'YYYY-MM-DD'));
INSERT INTO Shipping (order_id, customer_id, customer_address, order_date)
VALUES(13, 1, 'dawson college, montreal, qeuebe, canada', TO_DATE('2010-10-11', 'YYYY-MM-DD'));
INSERT INTO Shipping (order_id, customer_id, customer_address, order_date)
VALUES(14, 1, 'dawson college, montreal, qeuebe, canada', TO_DATE('2022-05-06', 'YYYY-MM-DD'));
INSERT INTO Shipping (order_id, customer_id, customer_address, order_date)
VALUES(15, 1, '104 gill street, Toronto, Canada', TO_DATE('2023-10-07', 'YYYY-MM-DD'));
INSERT INTO Shipping (order_id, customer_id, customer_address, order_date)
VALUES(16, 9, '105 Young street, toronto, canada', TO_DATE('2023-08-10', 'YYYY-MM-DD'));
INSERT INTO Shipping (order_id, customer_id, customer_address, order_date)
VALUES(17, 2, 'boul saint laurent, montreal, quebec, canada', TO_DATE('2023-10-23', 'YYYY-MM-DD'));
INSERT INTO Shipping (order_id, customer_id, customer_address, order_date)
VALUES(18, 2, 'boul saint laurent, montreal, quebec, canada', TO_DATE('2023-10-02', 'YYYY-MM-DD'));
INSERT INTO Shipping (order_id, customer_id, customer_address, order_date)
VALUES(19, 10, '87 boul saint laurent, montreal, quebec, canada', TO_DATE('2019-04-03', 'YYYY-MM-DD'));
INSERT INTO Shipping (order_id, customer_id, customer_address, order_date)
VALUES(20, 11, '76 boul decalthon, laval, quebec, canada', TO_DATE('29/12/2021', 'DD/MM/YYYY'));
INSERT INTO Shipping (order_id, customer_id, customer_address, order_date)
VALUES(21, 12, '22222 happy street, Laval, quebec, canada', TO_DATE('20/1/2020', 'DD/MM/YYYY'));
INSERT INTO Shipping (order_id, customer_id, customer_address, order_date)
VALUES(22, 1, 'dawson college, montreal, qeuebe, canada', TO_DATE('2022-10-11', 'YYYY-MM-DD'));
INSERT INTO Shipping (order_id, customer_id, customer_address, order_date)
VALUES(23, 11, '76 boul decalthon, laval, quebec, canada', TO_DATE('29/12/2021', 'DD/MM/YYYY'));

--Insert into Orders
INSERT INTO Orders (order_id,product_id,quantity)
VALUES(1,1,1);
INSERT INTO Orders (order_id,product_id,quantity)
VALUES(2,2,2);
INSERT INTO Orders (order_id,product_id,quantity)
VALUES(3,3,3);
INSERT INTO Orders (order_id,product_id,quantity)
VALUES(4,4,1);
INSERT INTO Orders (order_id,product_id,quantity)
VALUES(5,5,1);
INSERT INTO Orders (order_id,product_id,quantity)
VALUES(6,6,1);
INSERT INTO Orders (order_id,product_id,quantity)
VALUES(7,7,1);
INSERT INTO Orders (order_id,product_id,quantity)
VALUES(8,8,1);
INSERT INTO Orders (order_id,product_id,quantity)
VALUES(9,9,1);
INSERT INTO Orders (order_id,product_id,quantity)
VALUES(10,10,3);
INSERT INTO Orders (order_id,product_id,quantity)
VALUES(11,11,6);
INSERT INTO Orders (order_id,product_id,quantity)
VALUES(12,12,3);
INSERT INTO Orders (order_id,product_id,quantity)
VALUES(13,12,1);
INSERT INTO Orders (order_id,product_id,quantity)
VALUES(14,11,7);
INSERT INTO Orders (order_id,product_id,quantity)
VALUES(15,12,2);
INSERT INTO Orders (order_id,product_id,quantity)
VALUES(16,8,1);
INSERT INTO Orders (order_id,product_id,quantity)
VALUES(17,3,1);
INSERT INTO Orders (order_id,product_id,quantity)
VALUES(18,5,1);
INSERT INTO Orders (order_id,product_id,quantity)
VALUES(19,13,1);
INSERT INTO Orders (order_id,product_id,quantity)
VALUES(20,14,3);
INSERT INTO Orders (order_id,product_id,quantity)
VALUES(21,15,1);
INSERT INTO Orders (order_id,product_id,quantity)
VALUES(22,7,1);
INSERT INTO Orders (order_id,product_id,quantity)
VALUES(23,14,3);