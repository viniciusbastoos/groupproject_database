CREATE OR REPLACE PACKAGE orderPackage AS
        -- acts like a buy list before ordering it
    
    PROCEDURE addOrder(order_id IN Orders.order_id%TYPE, product_id IN Products.product_id%TYPE,quantity IN Orders.quantity%TYPE); 
        -- begins an order
    
    PROCEDURE addShipping(customer_id IN Shipping.customer_id%TYPE, customer_address IN Shipping.customer_address%TYPE, order_date IN Shipping.order_date%TYPE);
        -- Adds a new shipping  
        
    PROCEDURE modifyShipping(order_id1 IN Orders.order_id%TYPE, c_address IN Shipping.customer_address%TYPE);
        -- deletes all products related in the order and shipping
        -- deletes a specified product in the order list
      
    PROCEDURE deleteProductInOrder(orderid IN Orders.order_id%TYPE,productid IN Products.product_id%TYPE); 
        -- modifies the quantity of a product in a order list
        -- returns the total price of an order
        
    FUNCTION getTotalPrice(order_id1 IN Orders.order_id%TYPE) RETURN NUMBER;
        -- gets the full price of the cart
        
    PROCEDURE deleteAllOrders(order_id1 NUMBER);
        -- deletes all orders with the given order id
    
    PROCEDURE modifyQuantity(procedure_order_id NUMBER, procedure_product_id NUMBER, procedure_quantity NUMBER); 
        -- modifies quantity given the row and the new quantity. It serves as a fixing mistake.
    
END orderPackage;
/ 
CREATE OR REPLACE PACKAGE BODY orderPackage AS
    FUNCTION getNextOrderId RETURN Orders.order_id%TYPE AS 
        newid NUMBER;
    BEGIN
        SELECT COALESCE(MAX(order_id), 0) + 1 INTO newid
        FROM Shipping;
        RETURN newid;
    END getNextOrderId;
    
    
    
    PROCEDURE addOrder(order_id IN Orders.order_id%TYPE, product_id IN Products.product_id%TYPE, quantity IN Orders.quantity%TYPE) AS
    BEGIN
        INSERT INTO Orders(order_id, product_id, quantity)
        VALUES (order_id, product_id, quantity);
        COMMIT;
    END addOrder;
    
    

    PROCEDURE addShipping(customer_id IN Shipping.customer_id%TYPE, customer_address IN Shipping.customer_address%TYPE, order_date IN Shipping.order_date%TYPE)
    AS
        newid NUMBER;
    BEGIN
        newid := getNextOrderId;
        
        INSERT INTO Shipping(order_id, customer_id, customer_address, order_date)
        VALUES (newid, customer_id, customer_address, order_date);
        
        COMMIT;
    END addShipping;
    
    
    PROCEDURE modifyShipping(order_id1 IN Orders.order_id%TYPE, c_address IN Shipping.customer_address%TYPE) AS
    BEGIN
        UPDATE Shipping
        SET customer_address = c_address
        WHERE order_id = order_id1;
    END modifyShipping;
    


    PROCEDURE deleteProductInOrder(orderid IN Orders.order_id%TYPE, productid IN Products.product_id%TYPE) AS
    BEGIN
        DELETE FROM Orders
        WHERE order_id = orderid 
        AND product_id = productid;
        COMMIT;
    END deleteProductInOrder;
    
    
    
    FUNCTION getTotalPrice(order_id1 IN Orders.order_id%TYPE) RETURN NUMBER AS
        product# Products.product_id%TYPE;
        quantity Orders.quantity%TYPE; 
        pricetemp Products.product_price%TYPE;
        ordercount NUMBER; --counts total orderids
        total NUMBER := 0; --return variable
    BEGIN
        FOR line IN (SELECT product_id, quantity FROM Orders WHERE order_id = order_id1)
    LOOP
        product# := line.product_id;
        quantity := line.quantity;
        SELECT product_price INTO pricetemp 
        FROM Products WHERE product_id = product#;
        total := total+pricetemp*quantity;
    END LOOP;
        RETURN total;
    END getTotalPrice;
    
    
    PROCEDURE deleteAllOrders(order_id1 NUMBER) AS
    BEGIN
        DELETE FROM Orders o WHERE o.order_id = order_id1;
        DELETE FROM Shipping p WHERE p.order_id = order_id1;
    END deleteAllOrders;




    PROCEDURE modifyQuantity(procedure_order_id NUMBER, procedure_product_id NUMBER, procedure_quantity NUMBER) AS 
    BEGIN
        UPDATE Orders o SET o.Quantity = procedure_quantity 
        WHERE o.order_id = procedure_order_id AND o.product_id = procedure_product_id;
    END modifyQuantity;
END orderPackage;
/
--DECLARE
    --total NUMBER;
--BEGIN
    --orderPackage.addOrder(1,1,32); --orderid,productid,quantity
    --orderPackage.deleteProductInOrder(24,1);
    --orderPackage.addShipping(null,'Black Street', TO_DATE('29/12/2021', 'DD/MM/YYYY'));
    --total := orderPackage.getTotalPrice(1);
    --DBMS_OUTPUT.PUT_LINE('Total Price for Order 11: $' || total);
    --orderPackage.modifyQuantity(1,1,2);
    --orderPackage.deleteAllOrders(1);
    --orderpackage.modifyShipping(1, 'nowhere');
--END;