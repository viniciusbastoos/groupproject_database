CREATE OR REPLACE PACKAGE productPackage AS
    FUNCTION getReview(function_product_id NUMBER) RETURN NUMBER;
    FUNCTION checkQuantity(fproduct_id NUMBER) RETURN NUMBER;
    PROCEDURE addProduct(Pproduct_name VARCHAR2, Pproduct_price NUMBER, Pproduct_category VARCHAR2, Pproduct_review NUMBER);
    PROCEDURE currentQuantity(function_product_id NUMBER, quantityUsed NUMBER, operation NUMBER);
END productPackage;
/

CREATE OR REPLACE PACKAGE BODY productPackage AS
--Private function that determines the next id
    FUNCTION getNextIdProduct RETURN NUMBER
    AS 
        newid NUMBER;
    BEGIN
        SELECT COALESCE(MAX(product_id), 0) + 1 INTO newid
        FROM Products;
        RETURN newid;
    END getNextIdProduct;
    --gets average review rate
    FUNCTION getReview(function_product_id NUMBER) RETURN NUMBER AS
        review_result NUMBER;
    BEGIN
        SELECT MIN(product_review) INTO review_result
        FROM Products p
        WHERE p.product_id = function_product_id;
    
        RETURN review_result;
    END getReview;

    FUNCTION checkQuantity(fproduct_id NUMBER) RETURN NUMBER AS
        quantity_result NUMBER;
    BEGIN
        SELECT quantity INTO quantity_result
        FROM Products 
        INNER JOIN Warehouses_Products USING(product_id)
        WHERE product_id = fproduct_id;
        
        RETURN quantity_result;
        
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                RETURN NULL;
    END checkQuantity;

    PROCEDURE addProduct(Pproduct_name VARCHAR2, Pproduct_price NUMBER, Pproduct_category VARCHAR2, Pproduct_review NUMBER) AS
        newProductId NUMBER;
    BEGIN
        newProductId := getNextIdProduct;
        INSERT INTO Products (product_id, product_name, product_price, product_category, product_review)
        VALUES (newProductId, Pproduct_name, Pproduct_price, Pproduct_category, Pproduct_review);
        COMMIT;
        EXCEPTION
            WHEN OTHERS THEN
                ROLLBACK;
                RAISE;
    END addProduct;
    --checks the currently available quantity of the product
    PROCEDURE currentQuantity(function_product_id NUMBER, quantityUsed NUMBER, operation NUMBER) AS
        currentQuantity NUMBER;
    BEGIN
        SELECT Quantity INTO currentQuantity FROM Warehouses_Products wp WHERE wp.product_id = function_product_id;

        IF operation = 1 THEN
            UPDATE Warehouses_Products wp SET Quantity = currentQuantity + quantityUsed WHERE wp.product_id = function_product_id;
        ELSIF operation = 0 THEN
            UPDATE Warehouses_Products wp SET Quantity = currentQuantity - quantityUsed WHERE wp.product_id = function_product_id;
        ELSE
            DBMS_OUTPUT.PUT_LINE('Invalid operation');
        END IF;
    END currentQuantity;
END productPackage;
/