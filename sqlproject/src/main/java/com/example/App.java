package com.example;

import java.sql.*;
import java.util.InputMismatchException;
import java.util.Scanner;

public class App {
    private static Connection con;
    private static Scanner scanner = new Scanner(System.in); // Declare the Scanner at the class level

    public static void connection() throws SQLException {

        System.out.println("User: ");
        String user = scanner.next();
        System.out.println("Enter password: ");
        String pass = scanner.next();

        try {
            setConnection(user, pass);
            System.out.println("Connection successful. Welcome!");
        } 
        catch (SQLException e) {
            System.out.println("Connection failed. Please check your credentials and try again.");
            throw e;
        }
    }

    public static void menu() throws SQLException {
        Customer customer = new Customer(con);
        Order order = new Order(con);
        Product product = new Product(con);

        boolean validChoice = false;

        while (!validChoice) {
            // Prompt the user to choose a class
            System.out.println("Please choose a class to work on:");
            System.out.println("1. Customer Management");
            System.out.println("2. Orders Management");
            System.out.println("3. Product Management");
            System.out.println("4. Exit");

            try {
                int choice = scanner.nextInt();
                scanner.nextLine(); // Consume the newline character

                switch (choice) {
                    case 1:
                        customer.customerMenu();
                        //deleteCustomer();
                        validChoice = true;
                        break;
                    case 2:
                        order.orderMenu();
                        validChoice = true;
                        break;
                    case 3:
                        product.productMenu();
                        validChoice = true;
                        break;
                    case 4:
                        // Exit the loop and program
                        System.out.println("Thank you! Have a good day!");
                        validChoice = true;
                        break;
                    default:
                        System.out.println("Invalid choice. Please try again with numbers (1-4).");
                        break;
                }

            } 
            catch (InputMismatchException e) {
                // Check if input is int
                scanner.nextLine(); // avoid infinite loop
                System.out.println("Invalid input. Please enter a number (1-4).");
            }
        }
    }

    public static void setConnection(String user, String pass) throws SQLException {
        String url = "jdbc:oracle:thin:@198.168.52.211:1521/pdbora19c.dawsoncollege.qc.ca";
        con = DriverManager.getConnection(url, user, pass);
    }


    public static void main(String[] args) throws SQLException {
        try {
            connection();
            menu();
        } 
        catch (SQLException e) {
            System.out.println("Exiting the program due to connection failure.");
        } 
        finally {
            if (scanner != null) {
                scanner.close(); // Close the Scanner at the end of the main method
            }
        }
    }
}


/* 
        // Test modifyCustomer
        try {
            customer.modifyCustomer(1, "Jane Doe", "jane@example.com");
            System.out.println("modifyCustomer test passed.");
        } catch (SQLException e) {
            System.out.println("modifyCustomer test failed: " + e.getMessage());
        }

        // Test deleteCustomer
        try {
            customer.deleteCustomer(1);
            System.out.println("deleteCustomer test passed.");
        } catch (SQLException e) {
            System.out.println("deleteCustomer test failed: " + e.getMessage());
        }
        */