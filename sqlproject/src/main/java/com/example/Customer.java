// CLASS FOR ALL FUNCTIONS AND PROCEDURES OF customerPackage.sql

package com.example;
import java.sql.*;
import java.util.Scanner;

public class Customer {
    
    Scanner scanner = new Scanner(System.in);
    private Connection con;

    Customer(Connection con){
        this.con = con;
    }

    /*
     * METHODS TO CALL SQL FUNCTIONS AND PROCEDURES OF THE DATABASE
     */
    public void addCustomer(String name, String email) throws SQLException {
        try (CallableStatement stmt = con.prepareCall("{call customerPackage.addCustomer(?, ?)}")) {
            stmt.setString(1, name);   // Parameter 1 in the stored procedure
            stmt.setString(2, email);  // Parameter 2 in the stored procedure
            stmt.execute();
        }
    }
    
    public void modifyCustomer(int id, String name, String email) throws SQLException{
        try (CallableStatement stmt = con.prepareCall("{call customerPackage.modifyCustomer(?, ?, ?)}")) {
            stmt.setInt(1, id);
            stmt.setString(2, name);   // Parameter 1 in the stored procedure
            stmt.setString(3, email);  // Parameter 2 in the stored procedure
            stmt.execute();
        }
    }

    public void deleteCustomer(int id) throws SQLException{
        try (CallableStatement stmt = con.prepareCall("{call customerPackage.deleteCustomer(?)}")) {
            stmt.setInt(1, id);
            stmt.execute();
        }
    }

    /* 
     * MENU FOR CUSTOMER MANAGEMENT
    */
    public void customerMenu(){
        System.out.println("What would you like to do?");
        System.out.println("Add customer (1)");
        System.out.println("Modify customer (2)");
        System.out.println("Delete customer (3)");

        int asnwer = scanner.nextInt();

        if (asnwer == 1) {
            toAddCustomer();
        }
        else if (asnwer == 2) {
            toModifyCustomer();
        }
        else if (asnwer == 3){
            toDeleteCustomer();
        }
    }


    /*
     * TO METHODS ----------------------------------------------------------------------------------------------
     */

    /* PROCEDURE TO ADD A NEW CUSTOMER */
    private void toAddCustomer() {
        Customer customer = new Customer(con);

        scanner.nextLine();

        // Get customer details from the user
        System.out.println("Enter customer full name:");
        String name = scanner.nextLine();

        // Validate email format
        String email;
        while (true) {
            System.out.println("Enter customer email:");
            email = scanner.nextLine();
            if (isValidEmail(email)) {
                break;
            } 
            else {
                System.out.println("Invalid email format. Please enter a valid email.");
            }
        }

        // Test addCustomer
        try {
            addCustomer(name, email);
            System.out.println("Customer was fully added!");
            App.menu();
        } 
        catch (SQLException e) {
            System.out.println("Customer was not added. Reason: " + e.getMessage());
        }
    }


    /* PROCEDURE TO MODIFY AN EXISTING CUSTOMER */
    private void toModifyCustomer(){
        Customer customer = new Customer(con);

        scanner.nextLine();

        System.out.println("Enter customer ID:");
        int id = scanner.nextInt();

        scanner.nextLine();

        System.out.println("Enter customer full name:");
        String name = scanner.nextLine();

        // Validate email format
        String email;
        while (true) {
            System.out.println("Enter customer email:");
            email = scanner.nextLine();
            if (isValidEmail(email)) {
                break;
            } 
            else {
                System.out.println("Invalid email format. Please enter a valid email.");
            }
        }

        // Test modifyCustomer
        try {
            modifyCustomer(id, name, email);
            System.out.println("Customer was fully modified!");
            App.menu();

        } 
        catch (SQLException e) {
            System.out.println("Customer was not modified. Reason: " + e.getMessage());
        }
    }

    /* PROCEDURE TO DELETE A CUSTOMER */
    public void toDeleteCustomer(){
        Customer customer = new Customer(con);

        scanner.nextLine();

        System.out.println("Enter customer ID:");
        int id = scanner.nextInt();

        // Test modifyCustomer
        try {
            deleteCustomer(id);
            System.out.println("Customer was fully deleted!");
            App.menu();
        } 
        catch (SQLException e) {
            System.out.println("Customer was not deleted. Reason: " + e.getMessage());
        }
    }

    private static boolean isValidEmail(String email) {
        return email.contains("@");
    }
}
