// CLASS FOR ALL FUNCTIONS AND PROCEDURES OF orderPackage.sql

package com.example;
import java.sql.*;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Order {
    private Connection con;

    Order(Connection con){
        this.con = con;
    }

    /*
     * METHODS TO CALL SQL FUNCTIONS AND PROCEDURES OF THE DATABASE
     */
    public void addOrder(int oid, int pid, int quantity) throws SQLException {
        try (CallableStatement stmt = con.prepareCall("{call orderPackage.addOrder(?, ?, ?)}")) {
            stmt.setInt(1, oid);
            stmt.setInt(2, pid);
            stmt.setInt(3, quantity);
            stmt.execute();
        }
    }
    public void addShipping(int cid, String address, String date) throws SQLException {
        try (CallableStatement stmt = con.prepareCall("{call orderPackage.addShipping(?, ?, TO_DATE(?, 'DD/MM/YYYY'))}")) {
            stmt.setInt(1, cid);
            stmt.setString(2, address);
            stmt.setString(3, date);
            stmt.execute();
        }
    }
    public void modifyShipping(int cid, String address) throws SQLException {
        try (CallableStatement stmt = con.prepareCall("{call orderPackage.modifyShipping(?, ?)}")) {
            stmt.setInt(1, cid);
            stmt.setString(2, address);
            stmt.execute();
        }
    }
    public void deleteProductInOrder(int oid, int pid) throws SQLException {
        try (CallableStatement stmt = con.prepareCall("{call orderPackage.deleteProductInOrder(?, ?)}")) {
            stmt.setInt(1, oid);
            stmt.setInt(2, pid);
            stmt.execute();
        }
    }
    public String getTotalPrice(int oid) throws SQLException {
        try (CallableStatement callableStatement = con.prepareCall("{? = call orderPackage.getTotalPrice(?)}")) {
            callableStatement.registerOutParameter(1, java.sql.Types.NUMERIC);
            callableStatement.setInt(2, oid);
            callableStatement.execute();
            String returnValue = callableStatement.getString(1);
            return returnValue;
        }
    }
    public void deleteAllOrders(int oid) throws SQLException {
        try (CallableStatement stmt = con.prepareCall("{call orderPackage.deleteAllOrders(?)}")) {
            stmt.setInt(1, oid);
            stmt.execute();
        }
    }
    public void modifyQuantity(int oid, int pid, int quantity) throws SQLException {
        try (CallableStatement stmt = con.prepareCall("{call orderPackage.modifyQuantity(?, ?, ?)}")) {
            stmt.setInt(1, oid);
            stmt.setInt(2, pid);
            stmt.setInt(3, quantity);
            stmt.execute();
        }
    }


    /* 
     * MENU FOR ORDER MANAGEMENT
    */
    public void orderMenu() throws SQLException{
        Scanner key = new Scanner(System.in);

        System.out.println("What would you like to do?");
        System.out.println("Add a New Order (1)");
        System.out.println("Modify Shipping Address (2)");
        System.out.println("Delete Product In Order (3)");
        System.out.println("Show Table (4)");
        System.out.println("Delete All Orders (5)");
        System.out.println("Modify Quantity (6)");


        int asnwer = key.nextInt();
        if (asnwer == 1) {
            toAddOrder();
        }
        else if (asnwer == 2) {
            toModifyShipping();
        }
        else if (asnwer == 3){
            toDeleteProductInOrder();
        } 
        else if (asnwer == 4) {
            toShowTable();
        }
        else if (asnwer == 5){
           toDeleteAllOrders();
        } 
        else if (asnwer == 6) {
            toModifyQuantity();
        }
    }

    /*
     * TO METHODS ----------------------------------------------------------------------------------------------
     */

    /* PROCEDURE TO ADD A NEW ORDER */
    public void toAddOrder() throws SQLException{
        Scanner key = new Scanner(System.in);
        int customerid = 0;
        int orderid = getOrderId();
        String date = "";
        String address = "";
        System.out.println("On who's customer id will the order be?");
        while(true){
            try{
                customerid = key.nextInt();
                if(customerid <= 0){
                    System.out.println("Only positive numbers for the customer id");
                } 
                else {
                    break;
                }
            }catch(InputMismatchException e){
                System.out.println("Invalid input. Only positive numbers for the customer id");
                key.nextLine();
            }
        }
        key.nextLine(); // avoid jumping

        System.out.println("What will be the shipping address?");
        address = key.nextLine();
        while(true){
            System.out.println("What will be the delivery date? FORMAT = DD/MM/YYYY");
            date = key.next();
            if(date.matches("[0-9]{2}/[0-9]{2}/[0-9]{4}")) {
                break;
            } 
            else {
                System.out.println("Invalid date format. Please re-enter.");
            }
        }

        try {
            addShipping(customerid,address,date);
            System.out.println("Shipping was successfully added!");
        } 
        catch (Exception e) {
            System.out.println("Shipping was not added. Reason: " + e.getMessage());
            App.menu();
        }

        while(true){
            try{
                System.out.println("What productid would you like to order?");
                int productid = key.nextInt();
                System.out.println("What is its quantity?");
                int quantity = key.nextInt();
                if(productid <= 0 && quantity <= 0){
                    System.out.println("Only positive numbers");
                }
                else{
                    addOrder(orderid,productid,quantity);
                    System.out.println("Would you like to add another product in the order y/n?");
                    String response = key.next();
                    if(response.matches("n")){
                        App.menu();
                        break;
                    }
                }
            }
            catch(InputMismatchException e){
                System.out.println("Invalid input. Only positive numbers for the customer id");
                key.nextLine();
            }
        }
        key.close();
    }


    /* PROCEDURE TO MODIFY SHIPPING */
    public void toModifyShipping(){
        Scanner key = new Scanner(System.in);

        System.out.println("Please type the Order ID:");
        int orderID = key.nextInt();

        key.nextLine();

        System.out.println("Please type the Address:");
        String address = key.nextLine();

        try{
            modifyShipping(orderID, address);
            System.out.println("Shipping Address was successfully modified.");
            App.menu();

        }
        catch(SQLException e) {
            System.out.println("Shipping Address was not modified. Reason: " + e.getMessage());
        }
    }

    // PROCEDURE THAT SHOWS ALL PRODUCTS IN A SPECIFIC ORDER ID
    public void toShowTable() throws SQLException {
        Scanner key = new Scanner(System.in);
        int orderid = 0;
        System.out.println("What is the order id?");
        while (true) {
            try {
                orderid = key.nextInt();
                if (orderid <= 0) {
                    System.out.println("Only positive numbers for the order id");
                } 
                else {
                    break;
                }
            } catch (InputMismatchException e) {
                System.out.println("Invalid input. Only positive numbers for the order id");
                key.nextLine();
            }
        }
        key.nextLine(); // avoid jumping

        try (PreparedStatement stmt = con.prepareStatement("SELECT order_id, product_id, quantity FROM Orders WHERE order_id = ?")) {
            stmt.setInt(1, orderid); // Set the orderid parameter

            try (ResultSet resultSet = stmt.executeQuery()) {
                // Print column headers
                System.out.println("Total: " + getTotalPrice(orderid) + "$");
                System.out.println("Order# | Product# | Quantity");
                System.out.println("------------------------------");

                // Iterate through the result set and print each row
                while (resultSet.next()) {
                    int orderId = resultSet.getInt("order_id");
                    int productId = resultSet.getInt("product_id");
                    int quantity = resultSet.getInt("quantity");

                    // Print each row
                    System.out.println(orderId + " | " + productId + " | " + quantity);
                }
            }
        }
        App.menu();
    }
    /* PROCEDURE THAT DELETES A PRODUCT IN ORDER TABLE */
    public void toDeleteProductInOrder() throws SQLException{
        Scanner key = new Scanner(System.in);
        int orderid = 0;
        int productid = 0;
        System.out.println("What is the order id?");
        while(true){
            try{
                orderid = key.nextInt();
                if(orderid <= 0){
                    System.out.println("Only positive numbers for the order id");
                } 
                else {
                    break;
                }
            }catch(InputMismatchException e){
                System.out.println("Invalid input. Only positive numbers for the order id");
                key.nextLine();
            }
        }
        key.nextLine(); // avoid jumping
        System.out.println("What is the product id that you wish to remove from order#"+ orderid+"?");
         while(true){
            try{
                productid = key.nextInt();
                if(productid <= 0){
                    System.out.println("Only positive numbers for the product id");
                } 
                else {
                    break;
                }
            }catch(InputMismatchException e){
                System.out.println("Invalid input. Only positive numbers for the product id");
                key.nextLine();
            }
        }
        key.nextLine(); // avoid jumping
        try {
            deleteProductInOrder(orderid, productid);
            System.out.println("Product was successfully deleted from order!");
                    
        } catch (Exception e) {
            System.out.println("Product was not deleted from order. Reason:" + e.getMessage());
        }
        App.menu();
    }

    /* FUNCTION TO FETCH THE NEXT ORDER ID */
    public int getOrderId() throws SQLException{ 
        int rowCount = 0;
        try(PreparedStatement preparedStatement = con.prepareStatement("SELECT MAX(order_id) AS row_count FROM Shipping");

             ResultSet resultSet = preparedStatement.executeQuery()) {

            if (resultSet.next()) {
                rowCount = resultSet.getInt("row_count");
            } 
            else {
                System.out.println("No rows found in the Shipping table.");
            }
        } 
        catch (SQLException e) {
            e.printStackTrace();
            throw new SQLException("Error while counting rows in the Shipping table.", e);
        }

        return rowCount + 1;
    }


    /* PROCEDURE TO DELETE ALL ORDERS */
    public void toDeleteAllOrders(){
        Scanner key = new Scanner(System.in);

        System.out.println("Please type the Order ID:");
        int orderID = key.nextInt();

        key.nextLine();

        try{
            deleteAllOrders(orderID);
            System.out.println("The order was successfully deleted!.");
        }
        catch(SQLException e) {
            System.out.println("The order was not deleted. Reason: " + e.getMessage());
        }

        try {
            App.menu();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    /* PROCEDURE TO MODIFY QUANTITY OF PRODUCTS IN AN ORDER */
    public void toModifyQuantity(){
        Scanner key = new Scanner(System.in);

        System.out.println("Please type the Order ID:");
        int orderID = key.nextInt();

        System.out.println("Please type the Product ID:");
        int productID = key.nextInt();

        System.out.println("Please type the new quantity to modify:");
        int quantity = key.nextInt();      


        try{
            modifyQuantity(orderID, productID, quantity);
            System.out.println("The quantity was successfully modified!.");
        }
        catch(SQLException e) {
            System.out.println("The quantity was not modified. Reason: " + e.getMessage());
        }
        try {
            App.menu();
        } 
        catch (SQLException e) {
            e.printStackTrace();
        }
    } 
}
