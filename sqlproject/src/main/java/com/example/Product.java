// CLASS FOR ALL FUNCTIONS AND PROCEDURES OF productPackage.sql

package com.example;
import java.sql.*;
import java.util.Scanner;

public class Product {
    Scanner scanner = new Scanner(System.in);
    private Connection con;

    public Product(Connection con){
        this.con = con;
    }

    /*
     * METHODS TO CALL SQL FUNCTIONS AND PROCEDURES OF THE DATABASE
     */
    public int getReview(int product_id) throws SQLException {
        CallableStatement stmt = con.prepareCall("{ ? = call productPackage.getReview(?) }");
        stmt.registerOutParameter(1, Types.INTEGER);
        stmt.setInt(2, product_id);
        stmt.execute();
    
        int reviewResult = stmt.getInt(1);
        return reviewResult;
    }
    public int checkQuantity(int product_id) throws SQLException {
        CallableStatement stmt = con.prepareCall("{ ? = call productPackage.checkQuantity(?) }");
        stmt.registerOutParameter(1, Types.INTEGER);
        stmt.setInt(2, product_id);
        stmt.execute();
    
        int checkQuantity = stmt.getInt(1);
        return checkQuantity;
    }
    public void addProduct(String name, double price, String category, int review) throws SQLException{
        CallableStatement stmt = con.prepareCall("call productPackage.addProduct(?, ?, ?, ?)");
        stmt.setString(1,name);
        stmt.setDouble(2,price);
        stmt.setString(3,category);
        stmt.setInt(4,review);
        stmt.execute();
    }
    public void currentQuantity(int id, int quantity, int operation) throws SQLException{
        CallableStatement stmt = con.prepareCall("call productPackage.currentQuantity(?, ?, ?)");
        stmt.setInt(1,id);
        stmt.setInt(2,quantity);
        stmt.setInt(3,operation);
        stmt.execute();
    }


    /*
     * MENU FOR PRODUCTS MANAGEMENT
     */
    public void productMenu(){
        System.out.println("What would you like to do?");
        System.out.println("Get Review (1)");
        System.out.println("Check Quantity (2)");
        System.out.println("Add Product (3)");
        System.out.println("Current Quantity (4)");


        int asnwer = scanner.nextInt();

        if (asnwer == 1) {
            toGetReview();
        }
        else if (asnwer == 2) {
            toCheckQuantity();
        }
        else if (asnwer == 3) {
            toAddProduct();
        }
        else if (asnwer == 4) {
            toCurrentQuantity();
        }
    }


    /*
     * TO METHODS ----------------------------------------------------------------------------------------------
     */

    /* FUNCTION TO FETCH A REVIEW */
    private void toGetReview(){
        System.out.println("Enter product ID");
        int id = scanner.nextInt();

        try {
            int review = getReview(id);
            System.out.println("The review is: " + review);
            App.menu();
        } 
        catch (SQLException e) {
            System.out.println("The review was not able to be caught. Reason: " + e.getMessage());
        }

    }

    /* FUNCTION TO CHECK QUANTITY OF A PRODUCT IN WAREHOUSES_PRODUCTS */
    private void toCheckQuantity(){
        System.out.println("Enter product ID");
        int id = scanner.nextInt();

        try {
            int quantity = checkQuantity(id);
            System.out.println("The quantity is: " + quantity);
            App.menu();
        } 
        catch (SQLException e) {
            System.out.println("The quantity was not able to be caught. Reason: " + e.getMessage());
        }
}

    /* PROCEDURE TO ADD A NEW PRODUCT */
    private void toAddProduct(){
        System.out.println("Enter product name");
        String name = scanner.next();
        scanner.nextLine();
        System.out.println("Enter product price");
        double price = scanner.nextDouble();

        scanner.nextLine();

        System.out.println("Enter product category");
        String category = scanner.nextLine();

        System.out.println("Enter product review");
        int review = scanner.nextInt();

        try {
            addProduct(name, price, category, review);
            System.out.println("Product was fully added!");
            App.menu();
        } 
        catch (SQLException e) {
            System.out.println("Customer was not added. Reason: " + e.getMessage());
        }
    }

    /* PROCEDURE TO UPDATE QUANTITY OF A PRODUCT TO A NEW QUANTITY */
    private void toCurrentQuantity(){
        System.out.println("Enter product ID");
        int id = scanner.nextInt();

        System.out.println("Enter quantity used");
        int quantity = scanner.nextInt();

        System.out.println("Enter operation. Addition (1); Substraction (0)");
        int operation = scanner.nextInt();

        try {
            currentQuantity(id, quantity, operation);
            System.out.println("Quantity was successfully updated!");
            App.menu();
        } 
        catch (SQLException e) {
            System.out.println("Quantity was not updated. Reason: " + e.getMessage());
        }
    }
}
